const getBoardInformation = require("./callback1.cjs");
const getListsForBoard = require("./callback2.cjs");
const getCardsForLists = require("./callback3.cjs");

function getThanosInfo() {
    getBoardInformation("/home/rama/trello-callbacks.cjs/boards.json", "mcu453ed", (err, board) => {
        if (err) {
            console.log("Error:", err);
        } else {
            console.log(board);
            getListsForBoard("/home/rama/trello-callbacks.cjs/lists.json", board.id, (err, list) => {
                if (err) {
                    console.log("Error:", err);
                } else {
                    console.log(list);
                    getCardsForLists("/home/rama/trello-callbacks.cjs/cards.json", list[0].id, (err, card) => {
                        if (err) {
                            console.log("Error:", err);
                        } else {
                            console.log(card);
                        }
                    })
                }
            })
        }
    })
}

module.exports = getThanosInfo;