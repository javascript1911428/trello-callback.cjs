const getBoardInformation = require('./callback1.cjs');
const getListsForBoard = require('./callback2.cjs');
const getCardsForList = require('./callback3.cjs');

function getThanosInfo() {
  getBoardInformation('/home/rama/trello-callbacks.cjs/boards.json', 'mcu453ed', (err, board) => {
    if (err) {
      console.log('Error:', err);
    } else {
      console.log(board);

      getListsForBoard('/home/rama/trello-callbacks.cjs/lists.json', board.id, (err, lists) => {
        if (err) {
          console.log('Error:', err);
        } else {
          console.log(lists);

          const mindListId = lists[0].id;
          getCardsForList('/home/rama/trello-callbacks.cjs/cards.json', mindListId, (err, mindCards) => {
            if (err) {
              console.log('Error:', err);
            } else {
              const spaceListId = lists[1].id;
              getCardsForList('/home/rama/trello-callbacks.cjs/cards.json', spaceListId, (err, spaceCards) => {
                if (err) {
                  console.log('Error:', err);
                } else {
                  console.log([
                    { mindCards },
                    { spaceCards }
                  ]);
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = getThanosInfo;
