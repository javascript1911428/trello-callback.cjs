const fs = require('fs');

function getCardsForList(cardsData, listID, callback) {
    //console.log(cardsData);
    setTimeout(() => {
        fs.readFile(cardsData, 'utf8', (err, data) => {
            if (err) {
                callback(err)
            } else {
                let parsedBoardData = JSON.parse(data);
                //console.log(parsedBoardData);

                const cardsForListId = parsedBoardData[listID];
                //console.log(listsForBoard);
                callback(null, cardsForListId);

            }
        })
    }, 2000)
}

module.exports = getCardsForList;