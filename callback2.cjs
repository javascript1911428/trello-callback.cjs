const fs = require('fs');

function getListsForBoard(listsData, boardID, callback) {
    //console.log(listsData);
    setTimeout(() => {
        fs.readFile(listsData, 'utf8', (err, data) => {
            //console.log(data);
            //console.log(boardID)
            if (err) {
                callback(err)
            } else {
                let parsedBoardData = JSON.parse(data);
                //console.log(parsedBoardData);

                const listsForBoard = parsedBoardData[boardID];
                //console.log(listsForBoard);
                callback(null, listsForBoard);

            }
        })
    }, 2000)
}

module.exports = getListsForBoard;