const getBoardInformation = require('./callback1.cjs');
const getListsForBoard = require('./callback2.cjs');
const getCardsForList = require('./callback3.cjs');

function getThanosInfo() {
  getBoardInformation('/home/rama/trello-callbacks.cjs/boards.json', 'mcu453ed', (err, board) => {
    if (err) {
      console.log('Error:', err);
    } else {
      console.log('Board Information:', board);

      getListsForBoard('/home/rama/trello-callbacks.cjs/lists.json', board.id, (err, lists) => {
        if (err) {
          console.log('Error:', err);
        } else {
          console.log(lists);

          const allCardsInfo = [];
          let count = 0;

          lists.forEach((list) => {
            getCardsForList('/home/rama/trello-callbacks.cjs/cards.json', list.id, (err, cards) => {
              if (err) {
                console.log('Error:', err);
              } else {
                //count += 1;
                //console.log(count)
                if (typeof(cards) !== "undefined") {
                  //console.log(typeof(cards));
                  allCardsInfo.push({ list: list.name, cards });
                  count++;

                  if (count === lists.length - 1) {
                    console.log('All Cards for all Lists:', allCardsInfo);
                  }
                }
              }
            });
          });
        }
      });
    }
  });
}

module.exports = getThanosInfo;
