const fs = require('fs');

function getBoardInformation(boards, boardId, callback) {
    setTimeout(() => {
        fs.readFile(boards, "utf8", (err, data) => {
            //console.log(data);
            if (err) {
                callback(err)
            } else {
                let parsedBoardData = JSON.parse(data);
                let info = parsedBoardData.find(board => board['id'] === boardId);
                callback(null, info);
                }
        })
    }, 2000);
}
module.exports = getBoardInformation;



